package stackpackage;

public class mainClass {

	public static void main(String[] args) {
		Stack stack = new Stack();
		stack.push(10);
		stack.push(20);
		
		stack.show();
		System.out.println("\n" + stack.capacity);
		stack.push(30);
		stack.show();
		System.out.println("\n" + stack.capacity);
		stack.pop();
		stack.show();
		System.out.println("\n" + stack.capacity);
		stack.pop();
		stack.show();
		System.out.println("\n" + stack.capacity);
	}

}
