package stackpackage;

public class Stack {
	int capacity = 2;
	int[] stack = new int[2];
	int top;
	
	public void push(int data)
	{
		if(top == capacity)
		{
			expand();
		}
			stack[top] = data;
			top++;
	}
	
	private void expand() {
		int[] newStack = new int[capacity*2];
		System.arraycopy(stack, 0, newStack, 0, capacity);
		stack = newStack;
		capacity = capacity*2;
		newStack = null;
	}
	
	public void shrink()
	{
		int[] newStack = new int[capacity/2];
		capacity = capacity/2;
		System.arraycopy(stack, 0, newStack, 0, capacity);
		stack = newStack;
		newStack = null;
	}

	public int pop()
	{
		int value = 0;
		if(top-1 < (capacity/2))
		{
			shrink();	
		}
		if(top < 1)
		{
			System.out.println("Stack is Empty");
		}
		else
		{
			top--;
			value = stack[top];
			stack[top] = 0;
		}
		return value;
	}
	
	public int size()
	{
		return top;
	}
	
	public boolean isEmpty()
	{
		return top == 0;
	}

	public void show() {
		
		for(int data:stack)
		{
			System.out.print(data+" ");
		}
		
	}

}
