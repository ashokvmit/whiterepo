package circularlinkedlist;

public class mainClass {

	public static void main(String[] args) {
		
		CircularLinkedList list = new CircularLinkedList();
		
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		
		list.show();
		System.out.println("----------------");
		System.out.println(list.size());
		System.out.println("----------------");
		System.out.println(list.get(0));

	}

}
