package circularlinkedlist;

public class CircularLinkedList {

	Node head;
	
	int size()
	{
		Node n = head;
		int count = 0;
		while(n.next != head)
		{
			count = count+1;
			n = n.next;
		}
		count = count+1;
		return count;
	}
	
	int get(int index)
	{
		if(index >size()-1)
		{
			throw new ArrayIndexOutOfBoundsException(index);
		}
		Node n = head;
		int count = 0;
		while(n.next != head)
		{
			if(count == index)
			{
				break;
			}
			n = n.next;
			count = count+1;
		}
		
		return n.data;
	}
	
	
	void add(int data)
	{
		Node newNode = new Node(data);
		Node n = head;
		if(head == null)
		{
			 head = newNode;
			 head.next = head;
			 return;
		}
		while(n.next != head)
		{
			n = n.next;
		}
		newNode.next = head;
		n.next = newNode;
	}
	
	void delete(int data)
	{
		Node n = head;
		while(n.next != head)
		{
			n = n.next;
		}
	}
	
	void show()
	{
		Node n = head;
		while(true)
		{
				System.out.println(n.data + " " + n.next.data);
				
		if(n.next == head )
		{
			break;
		}
			n = n.next;
			
	}
	}
}
