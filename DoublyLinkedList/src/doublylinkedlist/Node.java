package doublylinkedlist;

public class Node {

	Node next;
	Node prev;
	int data;
	
	Node(){}
	Node(int data)
	{
		this.data = data;
	}
}
