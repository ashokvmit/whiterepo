package doublylinkedlist;

public class mainClass {

	public static void main(String[] args) {
		
		DoublyLinkedList list = new DoublyLinkedList();
		list.addAtFirst(20);
		list.addAtIndex(0, 10);
		list.addAtIndex(1, 40);
		list.addAtIndex(1,30);
		list.add(50);
		list.show();
		System.out.println("-------------------------");
		list.deleteAtIndex(2);
		list.show();

	}

}
