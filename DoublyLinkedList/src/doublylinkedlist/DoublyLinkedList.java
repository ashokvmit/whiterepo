package doublylinkedlist;

public class DoublyLinkedList {

	Node head;
	
	int size()
	{
		Node n = head;
		int count = 0;
		while(n != null)
		{
			n = n.next;
			count = count+=1;
		}
		return count;
	}
	
	void add(int data)
	{
		Node new_node  = new Node(data);
		new_node.next = null;
		
		if(head == null)
		{
			new_node.prev = null;
			head = new_node;
			return;
		}
		
		Node n = head;
		while(n.next != null)
		{
			n = n.next;
		}
		new_node.prev = n;
		n.next = new_node;
	}
	
	void addAtFirst(int data)
	{
		if(head == null)
		{
			Node newNode = new Node(data);
			head = newNode;
			head.prev = null;
			head.next = null;
		}
		else
		{
			Node newNode = new Node(data);
			newNode.prev = null;
			head.prev = newNode;
			newNode.next = head;
			head = newNode;
		}
	}
	
	void addAtIndex(int index, int data)
	{
		if(index>size())
		{
			throw new ArrayIndexOutOfBoundsException(index);
		}
		if(index == 0)
		{
			addAtFirst(data);
		}
		else
		{
			Node n = head;
			Node newNode = new Node(data);
			
			
		for(int i=0; i<index-1; i++)
		{
			n = n.next;
		}
		if(n.next !=null)
		{
			newNode.next = n.next;
			newNode.prev = n;
			n.next.prev = newNode;
			n.next = newNode;
		}
		else
		{
			newNode.next = null;
			newNode.prev = n;
			n.next = newNode;
		}
		}
	}
	
	int get(int data)
	{
		Node n = head;
		int count = 0;
		while(n.next != null)
		{
			count += 1;
			n = n.next;
			if(n.data == data)
			{
				break;
			}
		}
		return count;
	}
	
	
	void delete(int data)
	{
		Node n = head;
		
		if(data == head.data)
		{
			if(head.next == null)
			{
				head = null;
				return;
			}
			head = head.next;
			head.prev = null;
			return;
		}
	while(n != null)
	{
		if(n.data == data)
		{
			n.prev.next = n.next;
			n.next.prev = n.prev;
			n = null;
			break;
		}
		n = n.next;
	}
	
	}
	
	void deleteAtIndex(int index)
	{
		Node n = head;
		if(index > size()-1)
		{
			throw new ArrayIndexOutOfBoundsException(index);
		}
		if(index == 0)
		{
			delete(n.data);
		}
		else
		{	
		for(int i=0; i<index; i++)
		{
			n = n.next;
		}
		if(n.next == null)
		{
			n.prev.next = null;
			n = null;
		}
		else
		{
		n.prev.next = n.next;
		n.next.prev = n.prev;
		n = null;
		}
		}
	}
	
	void show()
	{
		Node n = head;
		while(n != null)
		{
			if(n.next == null &&  n.prev == null)
			{
				System.out.println(n.prev + " " + n.data + " " + n.next);
			}
			else if(n.prev != null && n.next != null)
			{
				System.out.println(n.prev.data + " " + n.data + " " + n.next.data);
			}
			else if(n.next != null)
			{
				System.out.println(n.prev + " " + n.data + " " + n.next.data);
			}
			else
			{
				System.out.println(n.prev.data + " " + n.data + " " + n.next);
			}
			n = n.next;
		}
	}
}
